import { createRouter, createWebHistory } from "vue-router";
import SettingPage from "../views/SettingPage.vue";

const routes = [
  {
    path: "/settings-page",
    name: "settings",
    props: true,
    component: SettingPage,
  },
  {
    path: "/game-board-page",
    name: "game-board",
    props: true,
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/GameBoardPage.vue"),
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
